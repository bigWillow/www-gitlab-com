---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2021-05-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 147       | 11.19%          |
| Based in EMEA                               | 373       | 28.39%          |
| Based in LATAM                              | 23        | 1.75%           |
| Based in NORAM                              | 771       | 58.68%          |
| **Total Team Members**                      | **1,314** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 888       | 67.58%          |
| Women                                       | 426       | 32.42%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **1,314** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 108       | 59.34%          |
| Women in Management                         | 74        | 40.66%          |
| Other Gender Management                     | 0         | 0%              |
| **Total Team Members**                      | **182**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 76        | 69.72%          |
| Women in Leadership                         | 33        | 30.28%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **109**   | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 443       | 79.82%          |
| Women in Engineering                        | 112       | 20.18%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **555**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.14%           |
| Asian                                       | 68        | 9.51%           |
| Black or African American                   | 21        | 2.94%           |
| Hispanic or Latino                          | 40        | 5.59%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.14%           |
| Two or More Races                           | 35        | 4.90%           |
| White                                       | 536       | 74.97%          |
| Unreported                                  | 13        | 1.82%           |
| **Total Team Members**                      | **715**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 20        | 10.20%          |
| Black or African American                   | 3         | 1.53%           |
| Hispanic or Latino                          | 6         | 3.06%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 11        | 5.61%           |
| White                                       | 155       | 79.08%          |
| Unreported                                  | 1         | 0.51%           |
| **Total Team Members**                      | **196**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 11        | 9.24%           |
| Black or African American                   | 4         | 3.26%           |
| Hispanic or Latino                          | 5         | 4.20%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 6.72%           |
| White                                       | 91        | 76.47%          |
| Unreported                                  | 0         | 0%              |
| **Total Team Members**                      | **119**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 15        | 17.24%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.45%           |
| White                                       | 66        | 75.86%          |
| Unreported                                  | 3         | 3.45%           |
| **Total Team Members**                      | **87**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.08%           |
| Asian                                       | 152       | 11.57%          |
| Black or African American                   | 36        | 2.74           |
| Hispanic or Latino                          | 73        | 5.56%           |
| Native Hawaiian or Other Pacific Islander   | 3         | 0.23%           |
| Two or More Races                           | 45        | 3.42%           |
| White                                       | 830       | 63.17%          |
| Unreported                                  | 174       | 13.24%          |
| **Total Team Members**                      | **1,314** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 65        | 11.71%          |
| Black or African American                   | 12        | 2.16%           |
| Hispanic or Latino                          | 28        | 5.05%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 17        | 3.06%           |
| White                                       | 337       | 60.72%          |
| Unreported                                  | 95        | 17.12%          |
| **Total Team Members**                      | **555**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 9.89%           |
| Black or African American                   | 4         | 2.20%           |
| Hispanic or Latino                          | 6         | 3.30%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 9         | 4.95%           |
| White                                       | 129       | 70.88%          |
| Unreported                                  | 16        | 8.79%           |
| **Total Team Members**                      | **182**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 16        | 14.68%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 0.92%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 2.75%           |
| White                                       | 77        | 70.64%          |
| Unreported                                  | 12        | 11.01%          |
| **Total Team Members**                      | **109**   | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 15        | 1.14%           |
| 25-29                                       | 199       | 15.14%          |
| 30-34                                       | 360       | 27.40%          |
| 35-39                                       | 319       | 24.28%          |
| 40-49                                       | 282       | 21.46%          |
| 50-59                                       | 118       | 8.98%           |
| 60+                                         | 21        | 1.60%           |
| Unreported                                  | 0         | 0.00%           |
| **Total Team Members**                      | **1,314** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
