---
layout: handbook-page-toc
title: "TPM.3.01 - Approved Service Provider Listing Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# TPM.3.01 - Approved Service Provider Listing

## Control Statement

GitLab maintains a list of approved, managed service providers and the services they provide to GitLab.

## Context

Maintaining a list of approved service providers will assist in validating exactly what a service provider offers.  Documentation should include:

* Provided service(s)
* What, if any, data is shared
* Data encryption utilized
* Date of last PCI compliance

## Scope

All externally sourced service providers utilized by GitLab that handles credit card data.

## Ownership

Control Owner:

* `Security Compliance`

Process Owner:

* Security Compliance

## Guidance

From the beginning of the relationship with the service provider, clearly document what service is being provided and what, if any, data is shared. Each service provider should provide an Attestation of Compliance to be included as evidence (AOC) each year (dated within 1 year of provided evidence date)

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Approved Service Provider Listing control issue](https://gitlab.com/gitlab-com/gl-security/security-assurance/sec-compliance/compliance/issues/930).

### Policy Reference

## Framework Mapping

* PCI
  * 12.8.1
