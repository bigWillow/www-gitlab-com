---
layout: handbook-page-toc
title: "GitLab Pitch Deck"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

GitLab Pitch Deck contains the primary GitLab overview presentation that tells the narrative of the company and the high-level problem/solution statements. 

## Audience 
This deck's narrative sits at the high-level. It is intended to be for c-level executives or walk-through sales pitches. 

## Location

GitLab Pitch Deck is a [google slides](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit) presentation. 

## Deck Maintenance 
- RACI: 
    - The pitch deck is maintained and kept up to date by the Brand Growth team. The responsible individual is the [Manader, Brand Growth](/job-families/marketing/corporate-events/#manager-brand-growth) with support from the [Manager, Product Marketing](/job-families/marketing/product-marketing-management/) who is accountable. The [CEO](/job-families/chief-executive-officer/) reviews the deck and must be consulted.  

- Subject Matter Experts: 
COMING SOON

- Update Cadence
  - The deck is reviewed quarterly 

## Additional Customer Facing Decks

Additional customer-facing decks and information about the process for supplemental materials can be found in the [Strategic Marketing Handbook](https://about.gitlab.com/handbook/marketing/strategic-marketing/#customer-facing-presentations)
