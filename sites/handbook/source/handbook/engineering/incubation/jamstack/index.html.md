---
layout: handbook-page-toc
title: Jamstack Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Jamstack Single-Engineer Group

The Jamstack SEG is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

This group aims to enable Front End developers to simplify their toolkit by using GitLab to manage, build, and deploy externally-facing, static websites using the Jamstack architecture.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329597]()